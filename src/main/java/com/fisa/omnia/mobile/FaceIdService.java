package com.fisa.omnia.mobile;

import java.awt.image.BufferedImage;

public interface FaceIdService {
    BufferedImage faceOf(String BufferedImage);

    boolean match(String first, String second);
}
