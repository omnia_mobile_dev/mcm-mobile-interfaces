package com.fisa.omnia.mobile;

import java.io.IOException;

public interface CloudMessageService {
    void susbscribe(Long userId, String token);
    String sendMessge(Long userId, String message) throws IOException;
}
