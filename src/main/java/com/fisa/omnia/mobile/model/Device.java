package com.fisa.omnia.mobile.model;

public class Device {
    private String type;
    private String uuid;
    private String manufacturer;
    private String model;
    private String command;
    private String base64face;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getBase64face() {
        return base64face;
    }

    public void setBase64face(String base64face) {
        this.base64face = base64face;
    }
}
