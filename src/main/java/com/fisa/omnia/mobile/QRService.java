package com.fisa.omnia.mobile;

import java.awt.image.BufferedImage;
import java.io.OutputStream;

public interface QRService {
    String pathOfQR(String var1, int var2) throws Exception;

    String base64OfQR(String var1, int var2) throws Exception;

    void streamOfQR(String var1, int var2, OutputStream var3) throws Exception;

    BufferedImage bufferedImageOfQR(String var1, int var2) throws Exception;
}


