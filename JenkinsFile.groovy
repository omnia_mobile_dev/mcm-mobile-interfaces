pipeline {
    agent any
    environment {
        mvnHome = tool 'M3'
        distribution_repo = 'http://192.168.0.198:8081/nexus/content/repositories/baninter-4.0.4-snapshot/'
        repo_id = 'baninter-4.0.4-snapshot'
    }
    stages {
        stage('build') {
            steps {

                script {
                    env.JAVA_HOME = "${tool 'JDK8'}"
                    env.PATH = "${env.JAVA_HOME}/bin:${env.PATH}"
                }
                dir('.') {
                    withMaven(
                            maven: 'M3',
                            mavenSettingsConfig: 'mobile2',mavenOpts:'-Xmx1024m') {
                        sh "mvn clean deploy -DskipTests -Dsource.skip -DaltDeploymentRepository=${repo_id}::default::${distribution_repo}"
                    }
                }
            }
        }
    }
    post {
        /*success {
            echo "COMMIT DEL DEPLOY: ${env.CONTROLLERS_COMMIT}"
        }*/
        failure {
            mail to: '/mobile_ii_bit@modinter.com.ec', subject: "[Jenkinsfile] omnia-controllers no compila correctamente.",
                    body: """Se debe revisar la compilación del proyecto, actualmente está generando errores:
            """
        }
    }
}
